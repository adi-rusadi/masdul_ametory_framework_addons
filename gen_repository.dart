// ignore_for_file: avoid_print
// dart run ./generator/gen_repository.dart --client test --method post --api coba_api --id

import 'dart:io';
import 'dart:async';

import 'gen.dart';

void main(List<String> args) async {
  print("test $args");
  await generateClient(args);
  return;
}

Future<void> generateClient(List<String> args) async {
  //snake
  String client = args[args.indexOf("--client") + 1];
  // ignore: unused_local_variable
  String clientUpperCamel = snakeToUpperCamel(client);
  String clientLowerCamel = snakeToLowerCamel(client);
  String clientLowerSnake = lowersnake(client);

  String method = args[args.indexOf("--method") + 1];
  // ignore: unused_local_variable
  String methodUpperCamel = snakeToUpperCamel(method);
  // ignore: unused_local_variable
  String methodLowerCamel = snakeToLowerCamel(method);
  // ignore: unused_local_variable
  String methodLowerSnake = lowersnake(method);

  String api = args[args.indexOf("--api") + 1];
  // ignore: unused_local_variable
  String apiUpperCamel = snakeToUpperCamel(api);
  // ignore: unused_local_variable
  String apiLowerCamel = snakeToLowerCamel(api);
  // ignore: unused_local_variable
  String apiLowerSnake = lowersnake(api);
  bool withId = false;
  if (args.contains("--id")) {
    withId = true;
  }

  print("client $clientLowerCamel");
  var directory = await Directory('lib/repository/$clientLowerSnake')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  if (!await File('${directory.path}/$clientLowerSnake' '_client.dart')
      .exists()) {
    String contentClient = await readFromFile(
        'generator/template/repository/sample_client.dart.template');

    contentClient = contentClient.replaceAll("Sample", clientUpperCamel);
    await writeToFile(
        content: contentClient,
        directory: directory,
        fileName: "$clientLowerSnake" "_client");
    print("client file generated");
  }

  String contentApi = await readFromFile(
      'generator/template/repository/sample_api_$method.dart.template');
  if (withId) {
    contentApi = contentApi.replaceAll(
        "SampleRequest request", "SampleRequest request, String id");
    contentApi = contentApi.replaceAll(
        "CanonicalPath.Sample", "'\${CanonicalPath.Sample}/\$id'");
  }
  contentApi = contentApi.replaceAll("Sample", apiUpperCamel);
  contentApi = contentApi.replaceAll("sample", apiLowerCamel);

  String original =
      await readFromFile("${directory.path}/${clientLowerSnake}_client.dart");
  var edited = original.replaceAll(
      "// generator event dont remove or modify this line", contentApi);
  // print(edited);
  await writeToFile(
      content: edited,
      directory: directory,
      fileName: "${clientLowerSnake}_client");
  print("client file modified");

  String contentRequest = await readFromFile(
      'generator/template/repository/sample_request.dart.template');
  contentRequest = contentRequest.replaceAll("Sample", apiUpperCamel);
  await writeToFile(
      content: contentRequest,
      directory: directory,
      fileName: "$apiLowerSnake" "_request");
  print("request file generated");

  String contentResponse = await readFromFile(
      'generator/template/repository/sample_response.dart.template');
  contentResponse = contentResponse.replaceAll("Sample", apiUpperCamel);
  await writeToFile(
      content: contentResponse,
      directory: directory,
      fileName: "$apiLowerSnake" "_response");
  print("Response file generated");

  print("\n\n");
}
