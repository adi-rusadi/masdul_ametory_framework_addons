// screen or widget optional
// dart run ./generator/gen_provider.dart account

// ignore_for_file: avoid_print, unused_local_variable

import 'dart:io';

import 'gen.dart';

void main(List<String> args) async {
  print("test $args");
  await generateProvider(args);
  await generatePreverence(args);
  await generateAppDart(args);
}

Future<void> generatePreverence(List<String> args) async {
  print("bloc preference generator start");
  var featureName = args[0];
  String featureLowerSnake = camelToLowerSnake(featureName);
  String featureUpperCamel = snakeToUpperCamel(featureLowerSnake);
  String featureLowerCamel = snakeToLowerCamel(featureLowerSnake);
  print("Proto $featureLowerSnake");
  var directory = await Directory('lib/common/providers/$featureLowerSnake')
      .create(recursive: true);
  print("directory.path ${directory.path}");

  String selectedContent =
      'generator/template/providers/sample_preference.dart.template';
  String contentEvent = await readFromFile(selectedContent);

  contentEvent = contentEvent.replaceAll(
      "SamplePreference", "${featureUpperCamel}Preference");
  contentEvent = contentEvent.replaceAll("Sample", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("sample", featureLowerCamel);

  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${featureLowerSnake}_preference");
}

Future<void> generateProvider(List<String> args) async {
  print("bloc preference generator start");
  var featureName = args[0];
  String featureLowerSnake = camelToLowerSnake(featureName);
  String featureUpperCamel = snakeToUpperCamel(featureLowerSnake);
  String featureLowerCamel = snakeToLowerCamel(featureLowerSnake);
  print("Proto $featureLowerSnake");
  var directory = await Directory('lib/common/providers/$featureLowerSnake')
      .create(recursive: true);
  print("directory.path ${directory.path}");

  String selectedContent =
      'generator/template/providers/sample_provider.dart.template';
  String contentEvent = await readFromFile(selectedContent);

  contentEvent = contentEvent.replaceAll(
      "SampleProvider", "${featureUpperCamel}Provider");
  contentEvent = contentEvent.replaceAll(
      "SamplePreverence", "${featureUpperCamel}Preverence");
  contentEvent = contentEvent.replaceAll(
      "samplePreverence", "${featureLowerCamel}Preverence");
  contentEvent = contentEvent.replaceAll(
      "sample_preverence", "${featureLowerSnake}Preverence");
  contentEvent = contentEvent.replaceAll("Sample", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("sample", featureLowerCamel);

  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${featureLowerSnake}_provider");
}


Future<void> generateAppDart(List<String> args) async {
  print("argument file generator start");

  var pageName = args[0];
  String pageLowerSnake = camelToLowerSnake(pageName);
  String pageNameUpper = snakeToUpperCamel(pageLowerSnake);
  String pageLowerCamel = snakeToLowerCamel(pageLowerSnake);
  print("Proto $pageLowerSnake");

  var directory = await Directory('lib/app').create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent =
      'generator/template/routes/router.dart.template';

  String original = await readFromFile("${directory.path}/app.dart");

  var edited = original.replaceAll(
      "// ADD PROVIDER INIT PLEASE DON'T REMOVE THIS LINE", "${pageNameUpper}Provider ${pageLowerCamel}Provider = ${pageNameUpper}Provider();\n\t// ADD PROVIDER INIT PLEASE DON'T REMOVE THIS LINE");

  edited = edited.replaceAll(
      "// ADD PROVIDER IMPORT PLEASE DON'T REMOVE THIS LINE",
      "import '../common/providers/$pageLowerCamel/${pageLowerCamel}_provider.dart';\n// ADD PROVIDER IMPORT PLEASE DON'T REMOVE THIS LINE");
  await writeToFile(
      content: edited, directory: directory, fileName: "app");
}
