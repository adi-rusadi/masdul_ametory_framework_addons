// ignore_for_file: avoid_print, unused_import
// sample
// dart run ./generator/gen.dart GetUser --feature home --handler Profile
import 'dart:io';
import 'dart:async';

void main(List<String> args) async {
  print("test $args");
  await generateState(args);
  await generateEvent(args);
  await generateBloc(args);

  return;
}

String snakeToUpperCamel(String string) {
  var list = string.split("_");
  return list
      .map((e) => e.substring(0, 1).toUpperCase() + e.substring(1))
      .join();
}

String snakeToLowerCamel(String string) {
  var list = string.split("_");
  return list
      .map((e) =>
          (list.indexOf(e) == 0
              ? e.substring(0, 1).toLowerCase()
              : e.substring(0, 1).toUpperCase()) +
          e.substring(1))
      .join();
}

String camelToLowerSnake(String string) {
  RegExp exp = RegExp(r'(?<=[a-z])[A-Z]');
  String result = string
      .replaceAllMapped(exp, (Match m) => ('_${m.group(0)!}'))
      .toLowerCase();
  return result;
}

String lowersnake(String string) {
  var list = string.split("_");
  return list.map((e) => e.toLowerCase()).join("_");
}

Future<String> readFromFile(path) async {
  String content = await File(path).readAsString().then((String contents) {
    return contents;
  });
  return content;
}

Future<void> writeToFile({
  required String content,
  required Directory directory,
  required String fileName,
}) async {
  // ignore: unused_local_variable
  var file =
      await File("${directory.path}/$fileName.dart").writeAsString(content);
}

Future<void> generateState(List<String> args) async {
  print("state file generator start");
  var protoName = args[0];
  String protoLowerSnake = camelToLowerSnake(protoName);
  String protoNameUpper = snakeToUpperCamel(protoLowerSnake);
  // ignore: unused_local_variable
  String protoLowerCamel = snakeToLowerCamel(protoLowerSnake);
  print("Proto $protoLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];
  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  print("feature $featureLowerCamel");

  var directory = await Directory('lib/feature/$featureLowerSnake/bloc/states')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String contentState =
      await readFromFile('generator/template/sample_state.dart.template');
  contentState = contentState.replaceAll("Feature", featureUpperCamel);
  contentState = contentState.replaceAll("Sample", protoNameUpper);
  // print(contentState);

  await writeToFile(
      content: contentState, directory: directory, fileName: protoLowerSnake);
  print("state file generated");
  print("\n\n");
}

Future<void> generateEvent(List<String> args) async {
  print("event file generator start");
  var protoName = args[0];
  String protoLowerSnake = camelToLowerSnake(protoName);
  String protoNameUpper = snakeToUpperCamel(protoLowerSnake);
  // ignore: unused_local_variable
  String protoLowerCamel = snakeToLowerCamel(protoLowerSnake);
  print("Proto $protoLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];
  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  print("feature $featureLowerCamel");

  var directory = await Directory('lib/feature/$featureLowerSnake/bloc')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String contentEvent =
      await readFromFile('generator/template/sample_event.dart.template');
  contentEvent = contentEvent.replaceAll("Feature", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("Sample", protoNameUpper);
  // print(contentEvent);

  String original =
      await readFromFile("${directory.path}/${featureLowerSnake}_event.dart");

  var edited = original.replaceAll(
      "// generator event dont remove or modify this line", contentEvent);
  // print(edited);
  await writeToFile(
      content: edited,
      directory: directory,
      fileName: "${featureLowerSnake}_event");
  print("event file modified");
  print("\n\n");
}

Future<void> generateBloc(List<String> args) async {
  print("bloc file generator start");
  var protoName = args[0];
  String protoLowerSnake = camelToLowerSnake(protoName);
  String protoNameUpper = snakeToUpperCamel(protoLowerSnake);
  String protoLowerCamel = snakeToLowerCamel(protoLowerSnake);
  print("Proto $protoLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];
  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  print("feature $featureLowerCamel");

  String handler = args[args.indexOf("--handler") + 1];
  String handlerUpperCamel = snakeToUpperCamel(handler);
  String handlerLowerCamel = snakeToLowerCamel(handler);
  // ignore: unused_local_variable
  String handlerLowerSnake = lowersnake(handler);
  print("handler $handlerLowerCamel");

  var directory = await Directory('lib/feature/$featureLowerSnake/bloc')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String contentEvent =
      await readFromFile('generator/template/sample_bloc.dart.template');
  contentEvent = contentEvent.replaceAll("Feature", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("Sample", protoNameUpper);
  contentEvent = contentEvent.replaceAll("sample", protoLowerCamel);
  contentEvent = contentEvent.replaceAll("Handler", handlerUpperCamel);
  contentEvent = contentEvent.replaceAll("handler", handlerLowerCamel);
  // print(contentEvent);

  String original =
      await readFromFile("${directory.path}/${featureLowerSnake}_bloc.dart");

  var edited = original.replaceAll(
      "// generator event dont remove or modify this line", contentEvent);
  print(edited);
  await writeToFile(
      content: edited,
      directory: directory,
      fileName: "${featureLowerSnake}_bloc");
  print("bloc file modified");
  print("\n\n");
}
