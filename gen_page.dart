// screen or widget optional
// dart run ./generator/gen_page.dart profileDetail --feature profile

// ignore_for_file: avoid_print, unused_local_variable

import 'dart:io';
import 'dart:async';

import 'gen.dart';

void main(List<String> args) async {
  print("test $args");
  await generateArgument(args);
  await generatePage(args);
  await generateScreen(args);
  await generateRoutes(args);
}

Future<void> generateArgument(List<String> args) async {
  print("argument file generator start");

  var pageName = args[0];
  String pageLowerSnake = camelToLowerSnake(pageName);
  String pageNameUpper = snakeToUpperCamel(pageLowerSnake);
  String pageLowerCamel = snakeToLowerCamel(pageLowerSnake);
  print("Proto $pageLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];

  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  var directory = await Directory('lib/feature/$featureLowerSnake/arguments')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent =
      'generator/template/pages/sample_arguments.dart.template';

  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");
  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${pageNameUpper}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${pageLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${pageLowerSnake}_screen");
  contentEvent = contentEvent.replaceAll("SamplePage", "${pageNameUpper}Page");
  contentEvent = contentEvent.replaceAll("samplePage", "${pageLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${pageLowerSnake}_page");
  contentEvent =
      contentEvent.replaceAll("SampleArguments", "${pageNameUpper}Arguments");
  contentEvent =
      contentEvent.replaceAll("sampleArguments", "${pageLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${pageLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", pageNameUpper);
  contentEvent = contentEvent.replaceAll("sample", pageLowerCamel);
  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${pageLowerSnake}_arguments");
}

Future<void> generatePage(List<String> args) async {
  print("argument file generator start");

  var pageName = args[0];
  String pageLowerSnake = camelToLowerSnake(pageName);
  String pageNameUpper = snakeToUpperCamel(pageLowerSnake);
  String pageLowerCamel = snakeToLowerCamel(pageLowerSnake);
  print("Proto $pageLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];

  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  var directory = await Directory('lib/feature/$featureLowerSnake/ui/page')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent = 'generator/template/pages/sample_page.dart.template';

  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");

  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${pageNameUpper}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${pageLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${pageLowerSnake}_screen");
  contentEvent = contentEvent.replaceAll("SamplePage", "${pageNameUpper}Page");
  contentEvent = contentEvent.replaceAll("samplePage", "${pageLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${pageLowerSnake}_page");
  contentEvent =
      contentEvent.replaceAll("SampleArguments", "${pageNameUpper}Arguments");
  contentEvent =
      contentEvent.replaceAll("sampleArguments", "${pageLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${pageLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", pageNameUpper);
  contentEvent = contentEvent.replaceAll("sample", pageLowerCamel);
  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${pageLowerSnake}_page");
}

Future<void> generateScreen(List<String> args) async {
  print("argument file generator start");

  var pageName = args[0];
  String pageLowerSnake = camelToLowerSnake(pageName);
  String pageNameUpper = snakeToUpperCamel(pageLowerSnake);
  String pageLowerCamel = snakeToLowerCamel(pageLowerSnake);
  print("Proto $pageLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];

  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  var directory = await Directory('lib/feature/$featureLowerSnake/ui/screen')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent =
      'generator/template/pages/sample_screen.dart.template';

  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");
  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${pageNameUpper}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${pageLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${pageLowerSnake}_screen");
  contentEvent = contentEvent.replaceAll("SamplePage", "${pageNameUpper}Page");
  contentEvent = contentEvent.replaceAll("samplePage", "${pageLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${pageLowerSnake}_page");
  contentEvent =
      contentEvent.replaceAll("SampleArguments", "${pageNameUpper}Arguments");
  contentEvent =
      contentEvent.replaceAll("sampleArguments", "${pageLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${pageLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", pageNameUpper);
  contentEvent = contentEvent.replaceAll("sample", pageLowerCamel);
  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${pageLowerSnake}_screen");
}

Future<void> generateRoutes(List<String> args) async {
  await generateRoutesHandler(args);
  await generateRoutesDefine(args);
}

Future<void> generateRoutesHandler(List<String> args) async {
  print("argument file generator start");

  var pageName = args[0];
  String pageLowerSnake = camelToLowerSnake(pageName);
  String pageNameUpper = snakeToUpperCamel(pageLowerSnake);
  String pageLowerCamel = snakeToLowerCamel(pageLowerSnake);
  print("Proto $pageLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];

  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  var directory = await Directory('lib/common/route').create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent =
      'generator/template/routes/router_handler.dart.template';

  String contentEvent = await readFromFile(selectedContent);

  contentEvent = contentEvent.replaceAll("Sample", pageNameUpper);
  contentEvent = contentEvent.replaceAll("sample", pageLowerCamel);
  String original = await readFromFile("${directory.path}/router_handler.dart");

  var edited = original.replaceAll(
      "// ADD ROUTE CONFIG PLEASE DON'T REMOVE THIS LINE", contentEvent);

  edited = edited.replaceAll(
      "// ADD ROUTE IMPORT PLEASE DON'T REMOVE THIS LINE",
      "import '../../feature/$featureLowerSnake/ui/page/${pageLowerSnake}_page.dart';\nimport '../../feature/$featureLowerSnake/arguments/${pageLowerSnake}_arguments.dart';\n// ADD ROUTE IMPORT PLEASE DON'T REMOVE THIS LINE");
  await writeToFile(
      content: edited, directory: directory, fileName: "router_handler");
}


Future<void> generateRoutesDefine(List<String> args) async {
  print("argument file generator start");

  var pageName = args[0];
  String pageLowerSnake = camelToLowerSnake(pageName);
  String pageNameUpper = snakeToUpperCamel(pageLowerSnake);
  String pageLowerCamel = snakeToLowerCamel(pageLowerSnake);
  print("Proto $pageLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];

  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  var directory = await Directory('lib/common/route').create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent =
      'generator/template/routes/router.dart.template';

  String contentEvent = await readFromFile(selectedContent);

  contentEvent = contentEvent.replaceAll("Sample", pageNameUpper);
  contentEvent = contentEvent.replaceAll("sample", pageLowerCamel);
  String original = await readFromFile("${directory.path}/router.dart");

  var edited = original.replaceAll(
      "// ADD HANDLER PLEASE DON'T REMOVE THIS LINE", contentEvent);

  edited = edited.replaceAll(
      "// ADD ROUTER PLEASE DON'T REMOVE THIS LINE",
      "static const String $pageLowerCamel = '/$pageLowerCamel';\n\t// ADD ROUTER PLEASE DON'T REMOVE THIS LINE");
  await writeToFile(
      content: edited, directory: directory, fileName: "router");
}
