// screen or widget optional
// dart run ./generator/gen_listener.dart DeleteUser --feature home --screen HomeScreen --widget HomeWidget

// ignore_for_file: avoid_print

import 'dart:io';
import 'dart:async';

import 'gen.dart';

void main(List<String> args) async {
  print("test $args");
  await generateScreen(args);
  await generateWidget(args);
}

Future<void> generateScreen(List<String> args) async {
  print("bloc file generator start");
  var protoName = args[0];
  String protoLowerSnake = camelToLowerSnake(protoName);
  String protoNameUpper = snakeToUpperCamel(protoLowerSnake);
  String protoLowerCamel = snakeToLowerCamel(protoLowerSnake);
  print("Proto $protoLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];
  // ignore: unused_local_variable
  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  print("feature $featureLowerCamel");
  if (!args.contains("--screen")) return;
  String screen = args[args.indexOf("--screen") + 1];

  String screenLowerSnake = camelToLowerSnake(screen);
  // ignore: unused_local_variable
  String screenUpperCamel = snakeToUpperCamel(screenLowerSnake);
  String screenLowerCamel = snakeToLowerCamel(screenLowerSnake);
  print("handler $screenLowerCamel");

  var directory = await Directory('lib/feature/$featureLowerSnake/ui/screen')
      .create(recursive: true);
  print("directory.path ${directory.path}");
  String selectedContent = 'generator/template/sample_listener.dart.template';
  if (args.contains("--list")) {
    selectedContent = 'generator/template/sample_listener_list.dart.template';
  }
  String contentEvent = await readFromFile(selectedContent);
  contentEvent = contentEvent.replaceAll("Sample", protoNameUpper);
  contentEvent = contentEvent.replaceAll("sample", protoLowerCamel);
  // print(contentEvent);

  String original =
      await readFromFile("${directory.path}/$screenLowerSnake.dart");

  var edited = original.replaceAll(
      "// generator event dont remove or modify this line", contentEvent);

  edited = _generateLoading(edited, protoNameUpper);
  edited = _generateVariable(args, edited, protoNameUpper, protoName);
  edited = _generateListener(edited, protoName);

  print(edited);
  await writeToFile(
      content: edited, directory: directory, fileName: screenLowerSnake);
  print("bloc file modified");
  print("\n\n");
}

Future<void> generateWidget(List<String> args) async {
  print("bloc file generator start");
  var protoName = args[0];
  String protoLowerSnake = camelToLowerSnake(protoName);
  String protoNameUpper = snakeToUpperCamel(protoLowerSnake);
  String protoLowerCamel = snakeToLowerCamel(protoLowerSnake);
  print("Proto $protoLowerSnake");

  String feature = args[args.indexOf("--feature") + 1];
  // ignore: unused_local_variable
  String featureUpperCamel = snakeToUpperCamel(feature);
  String featureLowerCamel = snakeToLowerCamel(feature);
  String featureLowerSnake = lowersnake(feature);
  print("feature $featureLowerCamel");
  if (!args.contains("--widget")) return;
  String widget = args[args.indexOf("--widget") + 1];

  String widgetLowerSnake = camelToLowerSnake(widget);
  // ignore: unused_local_variable
  String widgetUpperCamel = snakeToUpperCamel(widgetLowerSnake);
  String widgetLowerCamel = snakeToLowerCamel(widgetLowerSnake);
  print("handler $widgetLowerCamel");
  Directory directory;
  if (feature == "none") {
    directory = await Directory('lib/common/widgets').create(recursive: true);
  } else {
    directory = await Directory('lib/feature/$featureLowerSnake/ui/widget')
        .create(recursive: true);
  }

  print("directory.path ${directory.path}");
  String selectedContent = 'generator/template/sample_listener.dart.template';
  if (args.contains("--list")) {
    selectedContent = 'generator/template/sample_listener_list.dart.template';
  }
  String contentEvent = await readFromFile(selectedContent);
  contentEvent = contentEvent.replaceAll("Sample", protoNameUpper);
  contentEvent = contentEvent.replaceAll("sample", protoLowerCamel);
  // print(contentEvent);

  String original =
      await readFromFile("${directory.path}/$widgetLowerSnake.dart");

  var edited = original.replaceAll(
      "// generator event dont remove or modify this line", contentEvent);

  edited = _generateLoading(edited, protoNameUpper);
  edited = _generateVariable(args, edited, protoNameUpper, protoName);
  edited = _generateListener(edited, protoName);

  print(edited);
  await writeToFile(
      content: edited, directory: directory, fileName: widgetLowerSnake);
  print("bloc file modified");
  print("\n\n");
}

String _generateLoading(String edited, String protoNameUpper) {
  edited = edited.replaceAll(
    "// generator isLoading dont remove or modify this line",
    "// generator isLoading dont remove or modify this line\n"
        "is$protoNameUpper"
        "Loading ||\n",
  );
  return edited;
}

String _generateListener(String edited, String protoNameLowerCamel) {
  edited = edited.replaceAll(
    "// generator listener dont remove or modify this line",
    "// generator listener dont remove or modify this line\n"
        "_$protoNameLowerCamel"
        "Listener(context,state);",
  );
  return edited;
}

String _generateVariable(
    args, String edited, String protoNameUpper, String protoNameLower) {
  if (args.contains("--list")) {
    edited = edited.replaceAll(
      "// generator variable dont remove or modify this line",
      "bool is$protoNameUpper"
          "Loading = false;"
          "\n"
          "List<$protoNameUpper"
          "ResponseData> $protoNameLower"
          "Data = [];\n"
          "PaginationModel? $protoNameLower"
          "Pagination;\n"
          "// generator variable dont remove or modify this line",
    );
  } else {
    edited = edited.replaceAll(
      "// generator variable dont remove or modify this line",
      "bool is$protoNameUpper"
          "Loading = false;"
          "\n"
          "$protoNameUpper"
          "ResponseData? "
          "$protoNameLower"
          "Data;\n"
          "// generator variable dont remove or modify this line",
    );
  }
  return edited;
}
