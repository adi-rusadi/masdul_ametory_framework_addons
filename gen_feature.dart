// screen or widget optional
// dart run ./generator/gen_feature.dart profile

// ignore_for_file: avoid_print, unused_local_variable

import 'dart:io';
import 'dart:async';

import 'gen.dart';
import 'gen_page.dart';

void main(List<String> args) async {
  print("test $args");
  await generateBlocFile(args);
  await generateEventFile(args);
  await generateStateFile(args);
  await generateIndexFile(args);
  var newArgs = args.map((e) => e).toList();
  newArgs.add("--feature ${args[0]}");
  await generateArgument(newArgs);
  await generatePage(newArgs);
  await generateScreen(newArgs);
  await generateRoutes(newArgs);
}

Future<void> generateBlocFile(List<String> args) async {
  print("bloc file generator start");
  var featureName = args[0];
  String featureLowerSnake = camelToLowerSnake(featureName);
  String featureUpperCamel = snakeToUpperCamel(featureLowerSnake);
  String featureLowerCamel = snakeToLowerCamel(featureLowerSnake);
  print("Proto $featureLowerSnake");
  var directory = await Directory('lib/feature/$featureLowerSnake/bloc')
      .create(recursive: true);
  print("directory.path ${directory.path}");

  String selectedContent = 'generator/template/bloc/sample_bloc.dart.template';
  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");
  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${featureUpperCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${featureLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${featureLowerSnake}_screen");
  contentEvent =
      contentEvent.replaceAll("SamplePage", "${featureUpperCamel}Page");
  contentEvent =
      contentEvent.replaceAll("samplePage", "${featureLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${featureLowerSnake}_page");
  contentEvent = contentEvent.replaceAll(
      "SampleArguments", "${featureUpperCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sampleArguments", "${featureLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${featureLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("sample", featureLowerCamel);

  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${featureLowerSnake}_bloc");
}

Future<void> generateEventFile(List<String> args) async {
  print("bloc file generator start");
  var featureName = args[0];
  String featureLowerSnake = camelToLowerSnake(featureName);
  String featureUpperCamel = snakeToUpperCamel(featureLowerSnake);
  String featureLowerCamel = snakeToLowerCamel(featureLowerSnake);
  print("Proto $featureLowerSnake");
  var directory = await Directory('lib/feature/$featureLowerSnake/bloc')
      .create(recursive: true);
  print("directory.path ${directory.path}");

  String selectedContent = 'generator/template/bloc/sample_event.dart.template';
  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");
  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${featureUpperCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${featureLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${featureLowerSnake}_screen");
  contentEvent =
      contentEvent.replaceAll("SamplePage", "${featureUpperCamel}Page");
  contentEvent =
      contentEvent.replaceAll("samplePage", "${featureLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${featureLowerSnake}_page");
  contentEvent = contentEvent.replaceAll(
      "SampleArguments", "${featureUpperCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sampleArguments", "${featureLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${featureLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("sample", featureLowerCamel);

  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${featureLowerSnake}_event");
}

Future<void> generateStateFile(List<String> args) async {
  print("bloc file generator start");
  var featureName = args[0];
  String featureLowerSnake = camelToLowerSnake(featureName);
  String featureUpperCamel = snakeToUpperCamel(featureLowerSnake);
  String featureLowerCamel = snakeToLowerCamel(featureLowerSnake);
  print("Proto $featureLowerSnake");
  var directory = await Directory('lib/feature/$featureLowerSnake/bloc')
      .create(recursive: true);
  print("directory.path ${directory.path}");

  String selectedContent = 'generator/template/bloc/sample_state.dart.template';
  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");
  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${featureUpperCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${featureLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${featureLowerSnake}_screen");
  contentEvent =
      contentEvent.replaceAll("SamplePage", "${featureUpperCamel}Page");
  contentEvent =
      contentEvent.replaceAll("samplePage", "${featureLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${featureLowerSnake}_page");
  contentEvent = contentEvent.replaceAll(
      "SampleArguments", "${featureUpperCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sampleArguments", "${featureLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${featureLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("sample", featureLowerCamel);

  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "${featureLowerSnake}_state");
}

Future<void> generateIndexFile(List<String> args) async {
  print("bloc file generator start");
  var featureName = args[0];
  String featureLowerSnake = camelToLowerSnake(featureName);
  String featureUpperCamel = snakeToUpperCamel(featureLowerSnake);
  String featureLowerCamel = snakeToLowerCamel(featureLowerSnake);
  print("Proto $featureLowerSnake");
  var directory = await Directory('lib/feature/$featureLowerSnake/bloc')
      .create(recursive: true);
  print("directory.path ${directory.path}");

  String selectedContent = 'generator/template/bloc/index.dart.template';
  String contentEvent = await readFromFile(selectedContent);

  contentEvent =
      contentEvent.replaceAll("SampleBloc", "${featureUpperCamel}Bloc");
  contentEvent =
      contentEvent.replaceAll("sampleBloc", "${featureLowerCamel}Bloc");
  contentEvent.replaceAll("SampleEvent", "${featureUpperCamel}Event");
  contentEvent =
      contentEvent.replaceAll("sampleEvent", "${featureLowerCamel}Event");
  contentEvent =
      contentEvent.replaceAll("SampleState", "${featureUpperCamel}State");
  contentEvent =
      contentEvent.replaceAll("sampleState", "${featureLowerCamel}State");
  contentEvent =
      contentEvent.replaceAll("SampleScreen", "${featureUpperCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sampleScreen", "${featureLowerCamel}Screen");
  contentEvent =
      contentEvent.replaceAll("sample_screen", "${featureLowerSnake}_screen");
  contentEvent =
      contentEvent.replaceAll("SamplePage", "${featureUpperCamel}Page");
  contentEvent =
      contentEvent.replaceAll("samplePage", "${featureLowerCamel}Page");
  contentEvent =
      contentEvent.replaceAll("sample_page", "${featureLowerSnake}_page");
  contentEvent = contentEvent.replaceAll(
      "SampleArguments", "${featureUpperCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sampleArguments", "${featureLowerCamel}Arguments");
  contentEvent = contentEvent.replaceAll(
      "sample_arguments", "${featureLowerSnake}_arguments");
  contentEvent = contentEvent.replaceAll("Sample", featureUpperCamel);
  contentEvent = contentEvent.replaceAll("sample", featureLowerCamel);

  await writeToFile(
      content: contentEvent,
      directory: directory,
      fileName: "index");
}
